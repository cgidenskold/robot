# Robot
## How it works
### Move robot
Control movement of a "robot" inside a grid positioned by x/y-coordinates and direction.

Default position is X: 0, Y: 0, Direction: North (N).

* Use buttons in GUI to turn left, move forward and turn right.
* Enter a string of commands in input-field.
    * Supports both english and swedish commands.
    * L or V = Left (Vänster), F or G = Forward (Framåt), R or H = Right (Höger)
* Use keyboard arrows left, up and right to move robot inside grid.

### Settings
#### Grid
Grid can be customised in both the X and Y-range between -20 and 20.

#### Width/height
Width/height can be customised in the range between 100 and 500.

#### Restrictions
The grid supports two restrictions that limits movement for the robot.

##### Square
The robot can move anywhere inside the visible grid.

##### Circle
A circle is drawn inside the grid that will prevent the robot to move outside the circle.
When using circle as restriction, the X-axis and Y-axis is controlled using a single range slider. The same applies to width and height.

### Log
The movement of the robot is logged step for step on the left side of the page.  

## Available Scripts
This project is using React and was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `yarn flow`

Will run Flow-validation. Flow is a static type checker for javascript.

### `yarn test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.  
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
