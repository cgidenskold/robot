// @flow

// Libs
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import { RotateLeft, RotateRight, ArrowUpward, DirectionsRun } from '@material-ui/icons';

// Services
import { move, isPositionInGrid, getClosestPositionInSquareGrid } from '../services/robot';
import { translateCommands } from '../services/translate';

// Components
import Room from '../components/room';

// Views
import Settings from './settings';

// Contracts
import { GridRecord, PositionRecord, SizeRecord } from '../components/room/contracts';

// CSS
import './robot.css';
import 'react-input-range/lib/css/index.css';
import 'suitcss-utils-flex';
import 'suitcss-utils-text';

type State = {
    grid: GridRecord,
    position: PositionRecord,
    size: SizeRecord,
    log: Array<string>,
    commands: string,
};

class Robot extends Component<*, State> {
    // Set initial state. Instead of having the data below in state,
    // it could be stored in Redux and provided by props.
    // In a real situation, Redux would have been used.
    state = {
        grid: new GridRecord({ minX: -10, minY: -10, maxX: 10, maxY: 10, restriction: 'square' }),
        position: new PositionRecord({ x: 0, y: 0, dir: 'N' }),
        size: new SizeRecord({ width: 300, height: 300 }),
        log: [],
        commands: '',
    };

    /**
     * Store in log.
     */
    log = (entry: string) => {
        const { log } = this.state;

        log.unshift(entry);

        this.setState({ log });
    };

    /**
     * Move robot using commands and save movement to log.
     */
    move = (commands: string) => {
        const { grid } = this.state;
        let { position } = this.state;
        let newPosition;

        // Translate commands to always be in english.
        commands = translateCommands(commands);

        // Split up each character in commands and move one step at a time.
        commands.split('').forEach((command: string) => {
            // Check if valid command, else omit.
            if (!['L', 'F', 'R'].includes(command)) return;

            newPosition = move(grid, position, command);

            // If position hasn't chang
            if (newPosition.equals(position)) {
                this.log(`Edge reached at position: X: ${newPosition.get('x')} Y: ${newPosition.get('y')} D: ${newPosition.get('dir')}`);
            } else {
                this.log(`New position: X: ${newPosition.get('x')} Y: ${newPosition.get('y')} D: ${newPosition.get('dir')}`);
            }

            // Update current position for next step in loop.
            position = newPosition;
        });

        this.setState({
            position
        });
    };

    /**
     * Adds support to control robot by keyboard-keys left, up and right.
     * If required later, a throttle could also added to restrict key-repeat.
     */
    handleKeyDown = (e: KeyboardEvent) => {
        switch(e.key) {
            case 'ArrowLeft':
                e.preventDefault();
                this.move('L');
                break;
            case 'ArrowUp':
                e.preventDefault();
                this.move('F');
                break;
            case 'ArrowRight':
                e.preventDefault();
                this.move('R');
                break;
            default: // Do nothing
        }
    };

    /**
     * Set grid restriction.
     */
    setGridRestriction = (restriction: 'circle' | 'square') => {
        const { grid, size } = this.state;

        if (restriction === 'circle') {
            // Circular fence requires equal X/Y-axis and width/height.
            this.setState({
                grid: grid
                    .set('minY', grid.get('minX'))
                    .set('maxY', grid.get('maxX'))
                    .set('restriction', restriction),
                size: size.set('height', size.get('width')),
            });
        } else if (restriction === 'square') {
            this.setState({
                grid: grid.set('restriction', restriction),
            });
        }
    };

    /**
     * Update grid. Also check that position is within the grid and set new position if square-restriction.
     */
    setGrid = (grid: GridRecord) => {
        const { position } = this.state;

        // Check if position is within grid.
        if (isPositionInGrid(grid, position)) {
            // Safe to update grid.
            this.setState({ grid });
            return;
        }

        // Don't allow updating grid if position is outside grid if restriction circle.
        if (grid.get('restriction') === 'circle') {
            this.log('Grid update aborted. Robot at edge of circle.');
        }

        if (grid.get('restriction') === 'square') {
            // Get closest position.
            const closestPosition = getClosestPositionInSquareGrid(grid, position);

            // Position isn't within grid. Update position.
            this.setState({
                position: closestPosition,
            });

            this.log(`New position: X: ${closestPosition.get('x')} Y: ${closestPosition.get('y')} D: ${closestPosition.get('dir')}`);
        }
    };

    render = () => {
        const { grid, size, log } = this.state;

        return (
            <div tabIndex={ 0 } className='Robot u-flex' onKeyDown={ this.handleKeyDown }>
                <div className='Robot-logPanel u-flexGrow1' style={{ overflowY: 'auto' }}>
                    { log.map((entry: string, i: number) => (
                        <div key={ i }>{ entry }</div>
                    ))}
                </div>

                <div className='Robot-mainPanel u-flexGrow2'>
                    { this.renderMainPanel() }
                </div>

                <div className='Robot-settingsPanel u-flexGrow1'>
                    <Settings
                        grid={ grid }
                        size={ size }
                        setGrid={ (grid: GridRecord) => this.setGrid(grid) }
                        setSize={ (size: SizeRecord) => this.setState({ size }) }
                        setGridRestriction={ this.setGridRestriction }
                    />
                </div>
            </div>
        );
    };

    renderMainPanel = () => {
        const { grid, position, size, commands } = this.state;

        return (
            <div>
                <div className='Robot-mainPanelCurrentPosition u-textCenter'>
                    { `Robot at position: X: ${position.get('x')} Y: ${position.get('y')} D: ${position.get('dir')}` }
                </div>
                <Room
                    grid={ grid }
                    position={ position }
                    size={ size }
                    drawCircle={ grid.get('restriction') === 'circle' }
                    style={{ margin: '0 auto' }}
                />
                <div className='Robot-mainPanelButtons u-textCenter'>
                    <Button onClick={ () => this.move('L') } variant='contained' color='primary'>
                        <RotateLeft />
                    </Button>
                    <span style={{ margin: '0 15px' }}>
                        <Button onClick={ () => this.move('F') } variant='contained' color='primary'>
                            <ArrowUpward />
                        </Button>
                    </span>
                    <Button onClick={ () => this.move('R') } variant='contained' color='primary'>
                        <RotateRight />
                    </Button>
                </div>

                <div className='Robot-mainPanelCommands u-textCenter'>
                    <TextField
                        label='Commands'
                        placeholder='Enter multiple commands'
                        value={ commands }
                        helperText={ 'L or V = Left (Vänster), F or G = Forward (Framåt), R or H = Right (Höger)'}
                        onChange={ (e: SyntheticKeyboardEvent<*>) => {
                            this.setState({ commands: e.currentTarget.value.toUpperCase() });
                        } }
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position='end'>
                                    <IconButton
                                        onClick={ () => {
                                            this.move(commands);
                                            this.setState({ commands: '' });
                                        } }
                                    >
                                        <DirectionsRun/>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                </div>
            </div>
        )
    };
}

export default Robot;
