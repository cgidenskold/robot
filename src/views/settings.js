// @flow

// Libs
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import InputRange from 'react-input-range';

// Contracts
import { GridRecord, SizeRecord } from '../components/room/contracts';

// CSS
import './robot.css';
import 'react-input-range/lib/css/index.css';
import 'suitcss-utils-flex';
import 'suitcss-utils-text';

type Props = {
    grid: GridRecord,
    size: SizeRecord,
    setGrid: (grid: GridRecord) => any,
    setSize: (size: SizeRecord) => any,
    setGridRestriction: (restriction: 'square' | 'circle') => any,
};

class Settings extends Component<Props> {

    render = () => {
        const { grid, size, setGrid, setSize, setGridRestriction } = this.props;

        return (
            <div className='Robot-settingsPanel u-flexGrow1'>
                { grid.get('restriction') === 'circle' && (
                    <div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>X/Y-grid</div>
                            <InputRange
                                maxValue={ 20 }
                                minValue={ -20 }
                                value={{
                                    min: grid.get('minX'),
                                    max: grid.get('maxX'),
                                }}
                                onChange={ (v: { min: number, max: number }) => {
                                    setGrid(grid
                                        .set('minX', v.min)
                                        .set('maxX', v.max)
                                        .set('minY', v.min)
                                        .set('maxY', v.max)
                                    );
                                }}
                            />
                        </div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>Width/height</div>
                            <InputRange
                                maxValue={ 500 }
                                minValue={ 100 }
                                value={ size.get('width') }
                                onChange={ (v: number) => {
                                    setSize(size
                                        .set('width', v)
                                        .set('height', v)
                                    );
                                }}
                            />
                        </div>
                    </div>
                ) }

                { grid.get('restriction') === 'square' && (
                    <div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>X-grid</div>
                            <InputRange
                                maxValue={ 20 }
                                minValue={ -20 }
                                value={{
                                    min: grid.get('minX'),
                                    max: grid.get('maxX'),
                                }}
                                onChange={ (v: { min: number, max: number }) => {
                                    setGrid(grid
                                        .set('minX', v.min)
                                        .set('maxX', v.max)
                                    );
                                }}
                            />
                        </div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>Y-grid</div>
                            <InputRange
                                maxValue={ 20 }
                                minValue={ -20 }
                                value={{
                                    min: grid.get('minY'),
                                    max: grid.get('maxY'),
                                }}
                                onChange={ (v: { min: number, max: number }) => {
                                    setGrid(grid
                                        .set('minY', v.min)
                                        .set('maxY', v.max)
                                    );
                                }}
                            />
                        </div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>Width</div>
                            <InputRange
                                maxValue={ 500 }
                                minValue={ 100 }
                                value={ size.get('width') }
                                onChange={ (v: number) => {
                                    setSize(size.set('width', v));
                                }}
                            />
                        </div>
                        <div className='Robot-settingsPanelRangeSlider'>
                            <div style={{ paddingBottom: '20px' }}>Height</div>
                            <InputRange
                                maxValue={ 500 }
                                minValue={ 100 }
                                value={ size.get('height') }
                                onChange={ (v: number) => {
                                    setSize(size.set('height', v));
                                }}
                            />
                        </div>
                    </div>
                ) }
                <div className='u-textCenter' style={{ marginTop: '50px' }}>
                    { grid.get('restriction') === 'square' && (
                        <Button onClick={ () => setGridRestriction('circle') } variant='contained' color='primary'>
                            Set grid restriction to circle
                        </Button>
                    ) }
                    { grid.get('restriction') === 'circle' && (
                        <Button onClick={ () => setGridRestriction('square') } variant='contained' color='primary'>
                            Set grid restriction to square
                        </Button>
                    ) }
                </div>
            </div>
        );
    };
}

export default Settings;
