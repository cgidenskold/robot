import React, { Component } from 'react';

import Robot from './views/robot.js';

import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Robot />
            </div>
        );
    }
}

export default App;
