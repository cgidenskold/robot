// @flow

import { Record } from 'immutable';
import type { RecordFactory } from 'immutable';

/**
 * Position
 */
type Position = {
    x: number,
    y: number,
    dir: string,
}

export const PositionRecord: RecordFactory<Position> = Record({
    x: 0,
    y: 0,
    dir: 'N',
});

/**
 * Grid
 */
type Grid = {
    minX: number,
    minY: number,
    maxX: number,
    maxY: number,
    restriction: 'square' | 'circle',
};

export const GridRecord: RecordFactory<Grid> = Record({
    minX: 0,
    minY: 0,
    maxX: 0,
    maxY: 0,
    restriction: 'square',
});

/**
 * Size
 */
type Size = {
    width: number,
    height: number,
};

export const SizeRecord: RecordFactory<Size> = Record({
    width: 0,
    height: 0
});