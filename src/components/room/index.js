// @flow

import React, { PureComponent } from 'react';
import { GridRecord, PositionRecord, SizeRecord } from './contracts';

import './index.css';

type Props = {
    grid: GridRecord,
    position: PositionRecord,
    size: SizeRecord,
    drawCircle?: boolean,
    style?: Object,
};

class Room extends PureComponent<Props> {

    canvasNode: ?HTMLCanvasElement;
    positionCanvasNode: ?HTMLCanvasElement;

    componentDidMount = () => {
        const { drawCircle } = this.props;

        this.drawGrid();
        this.drawPosition();

        drawCircle && this.drawCircle();
    };

    /**
     * When component has been updated, re-draw canvaces if needed.
     */
    componentDidUpdate = (prevProps: Props) => {
        const { grid, position, size, drawCircle } = this.props;

        const gridChanged = !grid.equals(prevProps.grid);
        const positionChanged = !position.equals(prevProps.position);
        const sizeChanged = !size.equals(prevProps.size);

        if (gridChanged || sizeChanged) {
            this.drawGrid();
            drawCircle && this.drawCircle();
        }

        if (positionChanged || gridChanged || sizeChanged) {
            this.drawPosition();
        }
    };

    /**
     * Draw the grid inside canvasNode based on grid-settings and size-settings.
     */
    drawGrid = () => {
        const { grid, size } = this.props;

        const columns = grid.get('maxX') - grid.get('minX');
        const rows = grid.get('maxY') - grid.get('minY');

        const columnSize = (size.get('width') - 1) / columns;
        const rowSize = (size.get('height') - 1) / rows;

        const ctx = this.canvasNode && this.canvasNode.getContext('2d');

        if (!ctx || !this.canvasNode) return;

        // Clear canvas before draw.
        ctx.clearRect(0, 0, this.canvasNode.width, this.canvasNode.height);

        ctx.beginPath();
        ctx.strokeStyle = '#999';
        ctx.lineWidth = 1;

        // Draw columns.
        for (let x = 0; x <= size.get('width'); x += columnSize) {
            ctx.moveTo(0.5 + x, 0);
            ctx.lineTo(0.5 + x, size.get('height'));
        }

        // Draw rows.
        for (let y = 0; y <= size.get('height'); y += rowSize) {
            ctx.moveTo(0, 0.5 + y);
            ctx.lineTo(size.get('width'), 0.5 + y);
        }

        ctx.strokeStyle = '#999';
        ctx.stroke();
    };

    /**
     * Draw a circle inside canvasNode. Visualizes circular fence.
     */
    drawCircle = () => {
        const { size } = this.props;

        const ctx = this.canvasNode && this.canvasNode.getContext('2d');

        if (!ctx || !this.canvasNode) return;

        // Get shortest side and divide by 2 to get radius of circle.
        let radius = (size.get('width') > size.get('height') ? size.get('height') : size.get('width')) / 2;

        ctx.beginPath();
        ctx.arc(radius, radius, radius, 0, 2 * Math.PI);
        ctx.strokeStyle = '#FF0000';
        ctx.stroke();
    };

    /**
     * Draw the current position and direction in the grid.
     */
    drawPosition = () => {
        const { grid, position, size } = this.props;

        const ctx = this.positionCanvasNode && this.positionCanvasNode.getContext('2d');

        if (!ctx || !this.positionCanvasNode) return;

        // Clear canvas.
        ctx.clearRect(0, 0, this.positionCanvasNode.width, this.positionCanvasNode.height);

        // Calculate what x-coordinate position has in canvas.
        const columns = grid.get('maxX') - grid.get('minX');
        const widthMultiplier = (position.get('x') - grid.get('minX')) / columns;
        const x = widthMultiplier * size.get('width') - 1;

        // Calculate what y-coordinate position has in canvas.
        const rows = grid.get('maxY') - grid.get('minY');
        const heightMultiplier = (position.get('y') - grid.get('minY')) / rows;
        const y = size.get('height') - heightMultiplier * size.get('height') - 1;

        ctx.beginPath();

        // Draw arrow to visualize current direction.
        switch(position.get('dir')) {
            default:
            case 'N':
                ctx.moveTo(x - 3, y + 6);
                ctx.lineTo(x + 5, y + 6);
                ctx.lineTo(x + 1, y - 6);
                break;
            case 'W':
                ctx.moveTo(x + 4, y - 3);
                ctx.lineTo(x - 8, y + 1);
                ctx.lineTo(x + 4, y + 5);
                break;
            case 'S':
                ctx.moveTo(x + 5, y - 4);
                ctx.lineTo(x - 3, y - 4);
                ctx.lineTo(x + 1, y + 8);
                break;
            case 'E':
                ctx.moveTo(x - 4, y + 5);
                ctx.lineTo(x + 8, y + 1);
                ctx.lineTo(x - 4, y - 3);
                break;
        }

        ctx.fill();
    };

    render = () => {
        const { size, style } = this.props;

        return (
            <div className='Room' style={{ ...style, width: `${size.get('width')}px`, height: `${size.get('height')}px` }}>
                <canvas
                    ref={ (e: ?HTMLCanvasElement) => this.canvasNode = e }
                    width={ size.get('width') }
                    height={ size.get('height') }
                />
                <canvas
                    className='Room-positionCanvas'
                    ref={ (e: ?HTMLCanvasElement) => this.positionCanvasNode = e }
                    width={ size.get('width') }
                    height={ size.get('height') }
                />
            </div>
        );
    };
}

export default Room;
