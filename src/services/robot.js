// @flow

import { PositionRecord, GridRecord } from "../components/room/contracts";

/**
 * Determine if a specific position exists within a grid.
 * There can also exist a circular fence, calculate if position is within circle.
 */
export const isPositionInGrid = (grid: GridRecord, position: PositionRecord): boolean => {
    // If square restriction, just check if position is within grid.
    if (grid.get('restriction') === 'square') {
        return position.get('x') >= grid.get('minX') &&
            position.get('x') <= grid.get('maxX') &&
            position.get('y') >= grid.get('minY') &&
            position.get('y') <= grid.get('maxY');
    }

    if (grid.get('restriction') === 'circle') {
        // Circular fence is used, use pythagoras theorem to calculate distance.
        // The circle fence will always expect a grid with equal width/height and columns/rows.
        const columns = grid.get('maxX') - grid.get('minX');
        const rows = grid.get('maxY') - grid.get('minY');

        // Validate that columns/rows are equal, else fence is invalid.
        if (columns !== rows) {
            return false;
        }

        // Recalculate position x and y to get distance from center of circle.
        const circleRadius = columns / 2;
        const positionX = circleRadius + grid.get('minX') - position.get('x');
        const positionY = circleRadius + grid.get('minY') - position.get('y');

        // Use pythagoras to calculate if position is within circle.
        return Math.sqrt(Math.pow(positionX, 2) + Math.pow(positionY, 2)) <= circleRadius;
    }

    return false;
};

/**
 * If position is outside grid, calculate the closest position for appearin within grid.
 * Only for restriction square.
 */
export const getClosestPositionInSquareGrid = (grid: GridRecord, position: PositionRecord): PositionRecord => {
    // First check if position already is in grid, then just return position.
    if (isPositionInGrid(grid, position)) {
        return position;
    }

    // Position outside grid. Calculate closest position in restriction: square.
    if (grid.get('restriction') === 'square') {
        // X-axis
        if (grid.get('minX') > position.get('x')) {
            position = position.set('x', grid.get('minX'));
        } else if (grid.get('maxX') < position.get('x')) {
            position = position.set('x', grid.get('maxX'));
        }

        // Y-axis
        if (grid.get('minY') > position.get('y')) {
            position = position.set('y', grid.get('minY'));
        } else if (grid.get('maxY') < position.get('y')) {
            position = position.set('y', grid.get('maxY'));
        }
    }

    return position;
};

/**
 * Get new direction of robot after applied command.
 */
export const changeDirection = (direction: string, command: string): string => {
    const directions = ['N', 'E', 'S', 'W'];
    const directionIndex = directions.indexOf(direction);
    let newDirectionIndex;

    switch(command) {
        case 'L':
            newDirectionIndex = directionIndex === 0 ? 3 : directionIndex - 1;
            return directions[newDirectionIndex];
        case 'R':
            newDirectionIndex = directionIndex === directions.length - 1 ? 0 : directionIndex + 1;
            return directions[newDirectionIndex];
        default:
            return direction;
    }
};

/**
 * Make one move, unaware of constraints in grid.
 */
const forceSingleMove = (position: PositionRecord, command: string): PositionRecord => {
    // Only change direction.
    if (['L', 'R'].includes(command)) {
        return position.set('dir', changeDirection(position.get('dir'), command));
    }

    // Move forward, check in what direction.
    if (command === 'F') {
        switch(position.get('dir')) {
            case 'N':
                return position.set('y', position.get('y') + 1);
            case 'E':
                return position.set('x', position.get('x') + 1);
            case 'S':
                return position.set('y', position.get('y') - 1);
            case 'W':
                return position.set('x', position.get('x') - 1);
            default:
                return position;
        }
    }

    return position;
};

/**
 * Calculate position of robot after applying commands.
 */
export const move = (
    grid: GridRecord,
    startPosition: PositionRecord,
    commands: string,
): PositionRecord => {
    // Since commands my contain multiple movements, we want to move one position at a time.
    return commands.split('').reduce((acc, command) => {
        // Move position by force without limitations.
        const forcedPosition = forceSingleMove(acc, command);

        // Check if the new forced position is inside grid, else return old position.
        return isPositionInGrid(grid, forcedPosition) ? forcedPosition : acc;
    }, startPosition);
};