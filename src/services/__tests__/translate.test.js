import { translateCommands } from '../translate';

describe('Translate test', () => {
    it('should translate commands', () => {
        expect(translateCommands('VGHHGVV')).toEqual('LFRRFLL');
    });
});