import { PositionRecord, GridRecord } from '../../components/room/contracts';

import {
    move,
    changeDirection,
    isPositionInGrid,
    getClosestPositionInSquareGrid
} from '../robot';

// Default grid used for multiple tests.
const baseGrid = new GridRecord({
    minX: 0,
    minY: 0,
    maxX: 10,
    maxY: 10,
    restriction: 'square',
});

// Circular grid used for multiple tests.
const circularGrid = new GridRecord({
    minX: -10,
    minY: -10,
    maxX: 10,
    maxY: 10,
    restriction: 'circle',
});

// Circular offset grid used for multiple tests.
// Position (0, 0) not in middle of circle.
const circularOffsetGrid = new GridRecord({
    minX: -15,
    minY: -15,
    maxX: 4,
    maxY: 4,
    restriction: 'circle',
});

// Default start position used by multiple tests.
const baseStartPosition = new PositionRecord({
    x: 0,
    y: 0,
    dir: 'N',
});

describe('Robot test', () => {
    it('should change direction', () => {
        expect(changeDirection('N', 'L')).toEqual('W');
        expect(changeDirection('W', 'L')).toEqual('S');
        expect(changeDirection('S', 'L')).toEqual('E');
        expect(changeDirection('E', 'L')).toEqual('N');
        expect(changeDirection('N', 'R')).toEqual('E');
        expect(changeDirection('E', 'R')).toEqual('S');
        expect(changeDirection('S', 'R')).toEqual('W');
        expect(changeDirection('W', 'R')).toEqual('N');
    });

    it('should determine if position is in grid', () => {
        expect(isPositionInGrid(baseGrid, baseStartPosition)).toBeTruthy();
        expect(isPositionInGrid(baseGrid, new PositionRecord({ x: 5, y: 5 }))).toBeTruthy();
        expect(isPositionInGrid(baseGrid, new PositionRecord({ x: 11, y: 5 }))).toBeFalsy();
        expect(isPositionInGrid(baseGrid, new PositionRecord({ x: -1, y: 0 }))).toBeFalsy();
    });

    it('should turn left', () => {
        const expectedPosition = new PositionRecord({
            x: 0,
            y: 0,
            dir: 'W',
        });

        expect(move(baseGrid, baseStartPosition, 'L')).toEqual(expectedPosition);
    });

    it('should turn right', () => {
        const expectedPosition = new PositionRecord({
            x: 0,
            y: 0,
            dir: 'E',
        });

        expect(move(baseGrid, baseStartPosition, 'R')).toEqual(expectedPosition);
    });

    it('should move 1 step up on y-axis', () => {
        const expectedPosition = new PositionRecord({
            x: 0,
            y: 1,
            dir: 'N',
        });

        expect(move(baseGrid, baseStartPosition, 'F')).toEqual(expectedPosition);
    });

    it('should move 2 steps up on y-axis', () => {
        const expectedPosition = new PositionRecord({
            x: 0,
            y: 2,
            dir: 'N',
        });

        expect(move(baseGrid, baseStartPosition, 'FF')).toEqual(expectedPosition);
    });

    it('should move 1 step right on x-axis', () => {
        const expectedPosition = new PositionRecord({
            x: 1,
            y: 0,
            dir: 'E',
        });

        expect(move(baseGrid, baseStartPosition, 'RF')).toEqual(expectedPosition);
    });

    it('should move a combination of steps', () => {
        const expectedPosition = new PositionRecord({
            x: 5,
            y: 2,
            dir: 'E',
        });

        expect(move(baseGrid, baseStartPosition, 'RFFFFLFFRF')).toEqual(expectedPosition);
    });

    it('should not move past grid', () => {
        const expectedPosition = new PositionRecord({
            x: 10,
            y: 0,
            dir: 'E',
        });

        expect(move(baseGrid, baseStartPosition, 'RFFFFFFFFFFFFFFFFFFF')).toEqual(expectedPosition);
    });

    it('should move on a circular scale', () => {
        const expectedPosition = new PositionRecord({
            x: -3,
            y: -4,
            dir: 'S',
        });

        expect(move(circularGrid, baseStartPosition, 'LFFFLFFFF')).toEqual(expectedPosition);
    });

    it('should not move past grid on a circular scale', () => {
        const expectedPosition = new PositionRecord({
            x: -3,
            y: -9,
            dir: 'S',
        });

        expect(move(circularGrid, baseStartPosition, 'LFFFLFFFFFFFFFFFFFFFFFF')).toEqual(expectedPosition);
    });

    it('should move on a circular scale with offset grid', () => {
        const expectedPosition = new PositionRecord({
            x: -3,
            y: -4,
            dir: 'S',
        });

        expect(move(circularGrid, baseStartPosition, 'LFFFLFFFF')).toEqual(expectedPosition);
    });

    it('should not move past grid on a circular scale with offset grid', () => {
        const expectedPosition = new PositionRecord({
            x: 2,
            y: 0,
            dir: 'E',
        });

        expect(move(circularOffsetGrid, baseStartPosition, 'RFFFFFFFFFFFF')).toEqual(expectedPosition);
    });

    it('should get closest position inside grid if position is outside', () => {
        const outsidePosition = new PositionRecord({
            x: 30,
            y: 30,
            dir: 'E',
        });

        const expectedPosition = new PositionRecord({
            x: 10,
            y: 10,
            dir: 'E',
        });

        expect(getClosestPositionInSquareGrid(baseGrid, outsidePosition)).toEqual(expectedPosition);
    });
});