// @flow

/**
 * Both english and swedish commands should be supported.
 * Translate and only use english commands.
 */
export const translateCommands = (commands: string): string => {
    const mapper = {
        V: 'L',
        G: 'F',
        H: 'R',
    };

    return commands.replace(/[VGH]/g, m => mapper[m]);
};